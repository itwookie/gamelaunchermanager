#ifndef SYSTRAY_H
#define SYSTRAY_H

#include <windows.h>
#include <shellapi.h>
#include <commctrl.h>
#include <iostream>
#include <thread>

#include "AppTracker.h"

#define WM_MYMESSAGE (WM_USER + 1)

LRESULT CALLBACK ConsoleWndProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);

void MSG_Handler();

class SysTray {
    public:
        inline SysTray() {
            HINSTANCE hInstance = GetModuleHandle(nullptr);
            WNDCLASS wc;
            wc.cbClsExtra = 0;
            wc.cbWndExtra = 0;
            wc.hbrBackground = nullptr;
            wc.hCursor = nullptr;
            wc.hIcon = nullptr;
            wc.hInstance = hInstance;
            wc.lpfnWndProc = ConsoleWndProc;
            wc.lpszClassName = "__hidden__";
            wc.lpszMenuName = nullptr;
            wc.style = 0;
            RegisterClass(&wc);

            HWND hWnd = ::CreateWindowA("__hidden__","dummy", 0 , 0, 0, 100, 100, NULL, NULL, hInstance, NULL);
            //glpfnConsoleWindow = (WNDPROC)SetWindowLongPtrW(hWnd, GWLP_WNDPROC, (LONG_PTR)ConsoleWndProc);
            //CloseHandle(hInstance);

            iconActive   = (HICON)LoadImageW(nullptr, L"tray_e.ico", IMAGE_ICON, 0, 0, LR_LOADFROMFILE | LR_SHARED);
            iconInactive = (HICON)LoadImageW(nullptr, L"tray_d.ico", IMAGE_ICON, 0, 0, LR_LOADFROMFILE | LR_SHARED);

            notify.cbSize = sizeof(notify);
            notify.hWnd = hWnd;
            notify.uID = 42;
            notify.hIcon = iconActive;
            notify.dwState = 0;
            notify.dwStateMask = NIS_HIDDEN;
            notify.dwInfoFlags = NIIF_INFO;
            notify.uCallbackMessage = WM_MYMESSAGE;

            strcpy_s(notify.szTip, rsize_t(notify.szTip), "Game Launcher Manager");
            strcpy_s(notify.szInfoTitle, rsize_t(notify.szInfoTitle), "Game Launcher Manager");

            setState(true);

            notify.uFlags = NIF_ICON | NIF_TIP | NIF_STATE | NIF_INFO | NIF_MESSAGE;
            Shell_NotifyIconA(NIM_ADD, &notify);
        }
        inline ~SysTray() {
            //SetWindowLongPtrW(hWnd, GWLP_WNDPROC, (LONG_PTR)glpfnConsoleWindow);
            Shell_NotifyIconA(NIM_DELETE, &notify);
            DestroyIcon(iconActive);
            DestroyIcon(iconInactive);
        }

        static void popup(const char* msg) {
            strcpy_s(notify.szInfo, rsize_t(notify.szInfo), msg);
            Shell_NotifyIconA(NIM_MODIFY, &notify);
        }

        static void setState(bool state) {
            active = state;
            notify.hIcon = active ? iconActive : iconInactive;
            if (active) popup("Running");
            else popup("Inactive");
        }
    protected:

    private:
        static HWND hWnd;
        //static WNDPROC glpfnConsoleWindow;
        static HICON iconActive, iconInactive;
        static NOTIFYICONDATAA notify;
        static bool active;
};

#endif // SYSTRAY_H
