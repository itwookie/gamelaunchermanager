#ifndef APPFINDER_H
#define APPFINDER_H

#include <windows.h>
#include <tlhelp32.h>
#include <algorithm>
#include <string>
#include <iostream>
#include <iomanip>
#include <map>
#include <vector>

#include <iostream>

//default 2s
extern uint16_t TICK_DELAY;
//default 5 min = 300 sec @ TICK_DELAY 2s -> 150
extern uint32_t TICKS_TO_CLOSE;

class AppTracker {
    public:
        static inline void setupTicks(uint16_t tickSeconds, uint32_t ticksTerminate) {
            ::TICK_DELAY = tickSeconds;
            ::TICKS_TO_CLOSE = ticksTerminate;
        }

        /**
         * @param clientName the human readable name for this client
         * @param key the RegKey string pointing to the install after HKLM\Software\
         * @param value the actual entry name with the install path
         * @param exeLabel the name of the service executable in the install path
         * @param ignoredChildren name of service worker processes that'll always be
         *        spawned by the service application
         */
        inline AppTracker(const std::wstring clientName, const std::wstring key, const std::wstring value, const std::wstring exeLabel, std::initializer_list<std::wstring> ignoredChildren) {
            label = clientName;
            std::wstring baseKey1 = L"SOFTWARE\\";
            std::wstring baseKey2 = L"SOFTWARE\\Wow6432Node\\";
            baseKey1 += key;
            baseKey2 += key;
            if (getString(HKEY_LOCAL_MACHINE, baseKey1, value, exePath) ||
                getString(HKEY_LOCAL_MACHINE, baseKey2, value, exePath)
                ) {
                if (exePath.back()!='\\') exePath+='\\'; //ensure path ends with \ before appending executable name
                exePath += exeLabel;
                watchedExecutables.emplace(exePath, this);
                for (auto x : ignoredChildren) {
                    blacklist.push_back(x);
                }
            } else {
                exePath.resize(0);
            }
        }
        /**
         * @param key the RegKey string pointing to the install after HKLM\Software\
         * @param value the actual entry name with the install path (including the exe name)
         * @param ignoredChildren name of service worker processes that'll always be
         *        spawned by the service application
         */
        inline AppTracker(const std::wstring clientName, const std::wstring key, const std::wstring value, std::initializer_list<std::wstring> ignoredChildren) {
            label = clientName;
            std::wstring baseKey1 = L"SOFTWARE\\";
            std::wstring baseKey2 = L"SOFTWARE\\Wow6432Node\\";
            baseKey1 += key;
            baseKey2 += key;
            if (getString(HKEY_LOCAL_MACHINE, baseKey1, value, exePath) ||
                getString(HKEY_LOCAL_MACHINE, baseKey2, value, exePath)
                ) {
                watchedExecutables.emplace(exePath, this);
                for (auto x : ignoredChildren) {
                    blacklist.push_back(x);
                }
            } else {
                exePath.resize(0);
            }
        }
        inline ~AppTracker() {
            if (exePath.size()>0)
                for (auto it=watchedExecutables.begin(); it!=watchedExecutables.end(); ++it) {
                    if (it->second==this) {
                        watchedExecutables.erase(it);
                        break;
                    }
                }
        }

        inline std::wstring getExePath() const {
            return exePath;
        }
        /** @return the PID or 0 (system reserved ID) if app is not running */
        inline DWORD getPID() const {
            return pid;
        }
        inline void setPID(DWORD pid) {
            this->pid = pid;
        }
        inline std::wstring getLabel() const {
            return label;
        }

        bool isRunning() const;
        bool terminate();
        bool tick();

        inline void clearChildPIDs() { childPIDs.clear(); }
        inline void addChildPID(std::wstring processName, DWORD pid) {
            if (!isBlacklisted(processName))
                childPIDs.emplace_hint(childPIDs.begin(), processName, pid);
        }

        inline bool isSticky() {
            return sticky;
        }
        inline void setSticky(bool sticky) {
            this->sticky = sticky;
        }

        static bool rescan();
        static bool getTracker(const std::wstring name, AppTracker** instance);

        friend std::wostream& operator<< (std::wostream& stream, const AppTracker& app) {
            stream << "[ " << std::setw(5) << std::setfill(L' ') << app.getPID() << " ] " << app.getLabel();
            if (app.sticky) stream << " *";
            else if (app.appIdleTicks>0) stream << " t-" << (::TICKS_TO_CLOSE-app.appIdleTicks)*::TICK_DELAY << 's';
            stream << std::endl;
            for (auto& x : app.childPIDs) {
                stream << "  [ " << std::setw(5) << std::setfill(L' ') << x.second << " ] " << x.first << std::endl;
            }
            return stream;
        }
        /** @return the const iterator over all <wstring, AppTracker> target application <-> tracker */
        static std::map<std::wstring, AppTracker*>& getAllTracker();
        static bool toggleTracking();

    protected:
        static std::map<std::wstring, AppTracker*> watchedExecutables;

    private:
        DWORD pid = 0;
        std::wstring exePath;
        std::wstring label;
        std::map<std::wstring, DWORD> childPIDs;
        std::vector<std::wstring> blacklist;
        uint32_t appIdleTicks = 0;
        bool sticky = false;
        static bool isTracking;

        inline bool isBlacklisted(std::wstring e) {
            for (auto& x : blacklist) {
                if (e.compare(x)==0) return true;
            }
            return false;
        }

        //used to resolve registry values
        inline bool getString(HKEY hKey, const std::wstring& subKey, const std::wstring& value, std::wstring& result) {
            //std::wcout << L"Searching at " << subKey << L"\\" << value << std::endl;
            DWORD dataSize{};
            LONG retCode = ::RegGetValueW( hKey, subKey.c_str(), value.c_str(), RRF_RT_REG_SZ, nullptr, nullptr, &dataSize );
            if (retCode != ERROR_SUCCESS)
                return false;
            result.resize(dataSize / sizeof(wchar_t));
            retCode = ::RegGetValueW( hKey, subKey.c_str(), value.c_str(), RRF_RT_REG_SZ, nullptr, &result[0], &dataSize );
            if (retCode != ERROR_SUCCESS)
                return false;
            DWORD stringLengthInWchars = dataSize / sizeof(wchar_t);
            stringLengthInWchars--; // Exclude the NUL written by the Win32 API
            result.resize(stringLengthInWchars);

            return true;
        }
};

#endif // APPFINDER_H
