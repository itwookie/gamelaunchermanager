#ifndef MAIN_H
#define MAIN_H

#include <iostream>
#include <string>
#include <thread>
#include <chrono>

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>

#include "AppTracker.h"
#include "SysTray.h"

#endif // MAIN_H
