#include "AppTracker.h"

uint16_t TICK_DELAY;
uint32_t TICKS_TO_CLOSE;

std::map<std::wstring, AppTracker*> AppTracker::watchedExecutables;
bool AppTracker::isTracking = true;

bool AppTracker::isRunning() const {
    return getPID()!=0;
}

bool AppTracker::terminate() {
    HANDLE handle;
    handle = OpenProcess(PROCESS_TERMINATE, false, getPID());
    bool result = TerminateProcess(handle, 0);
    CloseHandle(handle);
    return result;
}

bool AppTracker::getTracker(const std::wstring name, AppTracker** instance) {
    size_t ml = name.size();
    for (auto& x : watchedExecutables) {
        if (_wcsnicmp(x.first.c_str(), name.c_str(), ml)==0) {
//            std::wcout << L"Returning tracker " << x.second << std::endl;
            *instance = x.second;
            return true;
        }
    }
    instance = nullptr;
    return false;
}

/** @return true if the app was closed during this tick */
bool AppTracker::tick() {
    if (!isTracking || sticky || !isRunning() || childPIDs.size() > 0) {
        appIdleTicks = 0;
    } else if (++appIdleTicks >= ::TICKS_TO_CLOSE) {
        appIdleTicks = 0;
        return terminate();
    }
    return false;
}

std::map<std::wstring, AppTracker*>& AppTracker::getAllTracker() {
    return watchedExecutables;
}

bool AppTracker::toggleTracking() {
    isTracking =! isTracking;
    return isTracking;
}

bool AppTracker::rescan() {
    for (auto& x : watchedExecutables) {
        x.second->setPID(0); //clear pids
    }
    std::map<DWORD, AppTracker*> linked;

    HANDLE hProcessSnap;
    PROCESSENTRY32 pe32;

    // Take a snapshot of all processes in the system.
    hProcessSnap = CreateToolhelp32Snapshot( TH32CS_SNAPPROCESS, 0 );
    if( hProcessSnap == INVALID_HANDLE_VALUE ) {
        return false;
    }
    // Set the size of the structure before using it.
    pe32.dwSize = sizeof( PROCESSENTRY32 );

    // Retrieve information about the first process,
    // and exit if unsuccessful
    if( !Process32First( hProcessSnap, &pe32 ) ) {
        CloseHandle( hProcessSnap );          // clean the snapshot object
        return false;
    }
    // Now walk the snapshot of processes, and
    // display information about each process in turn
    do {
        HANDLE hModuleSnap = INVALID_HANDLE_VALUE;
        MODULEENTRY32 me32;
        // Take a snapshot of all modules in the specified process.
        hModuleSnap = CreateToolhelp32Snapshot( TH32CS_SNAPMODULE, pe32.th32ProcessID );
        if( hModuleSnap != INVALID_HANDLE_VALUE ) { //fails if not permitted (e.g. scanning admin apps as non admin)
            // Set the size of the structure before using it.
            me32.dwSize = sizeof( MODULEENTRY32 );
            // Retrieve information about the first module,
            // and exit if unsuccessful
            if( Module32First( hModuleSnap, &me32 ) &&
               pe32.th32ProcessID == me32.th32ProcessID //is own host
               ) {
                //get module name as string
                std::string strModule(me32.szExePath);
                //convert to wstring
                int size_needed = MultiByteToWideChar(CP_UTF8, 0, &strModule[0], (int)strModule.size(), NULL, 0);
                std::wstring wstrModule( size_needed, 0 );
                MultiByteToWideChar(CP_UTF8, 0, &strModule[0], (int)strModule.size(), &wstrModule[0], size_needed);
                //Update AppFinder if registered
                AppTracker* tracker;
                if (AppTracker::getTracker(wstrModule, &tracker)) {
//                    std::wcout << L"Returned tracker " << tracker << ", setting PID " << pe32.th32ProcessID << ", parent " << pe32.th32ParentProcessID << std::endl;

                    HANDLE hParentModuleSnap = INVALID_HANDLE_VALUE;
                    hParentModuleSnap = CreateToolhelp32Snapshot( TH32CS_SNAPMODULE, pe32.th32ParentProcessID );
                    if( hParentModuleSnap == INVALID_HANDLE_VALUE ) { // no valid parent - process running on it's own
                        tracker->clearChildPIDs();
                        tracker->setPID(me32.th32ProcessID);
                        linked.emplace(me32.th32ProcessID, tracker);
                    } else {
                        MODULEENTRY32 pme32;
                        if( Module32First( hParentModuleSnap, &pme32 ) && //look at parent module
                           _strnicmp(pme32.szModule, me32.szModule, strlen(me32.szModule)) != 0 //parent exe is equal to child exe
                           ) { // parent is not same exe as this process
                            tracker->clearChildPIDs();
                            tracker->setPID(me32.th32ProcessID);
                            linked.emplace(me32.th32ProcessID, tracker);
                        }
                    }
                    CloseHandle( hParentModuleSnap );
                }
            }
            CloseHandle( hModuleSnap );           // clean the snapshot object
        }

    } while( Process32Next( hProcessSnap, &pe32 ) );

    // re-walk the snapshot to register child applications
    if( !Process32First( hProcessSnap, &pe32 ) ) {
        CloseHandle( hProcessSnap );          // clean the snapshot object
        return false;
    }

    // Now walk the snapshot of processes, and
    // display information about each process in turn
    do
    {
        auto it = linked.find(pe32.th32ParentProcessID);
        if (it != linked.end()) {
            //get process name as string
            std::string strProccess(pe32.szExeFile);
            //convert to wstring
            int size_needed = MultiByteToWideChar(CP_UTF8, 0, &strProccess[0], (int)strProccess.size(), NULL, 0);
            std::wstring wstrProcess( size_needed, 0 );
            MultiByteToWideChar(CP_UTF8, 0, &strProccess[0], (int)strProccess.size(), &wstrProcess[0], size_needed);

            (*it).second->addChildPID(wstrProcess, pe32.th32ProcessID);
        }
    } while( Process32Next( hProcessSnap, &pe32 ) );

    CloseHandle( hProcessSnap );

    return true;
}
