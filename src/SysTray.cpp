#include "SysTray.h"

LRESULT CALLBACK ConsoleWndProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam) {
    //std::wcout << "MSG " << msg << " lParam " << lParam << std::endl;
    switch (msg) {
    case WM_MYMESSAGE:
        switch(lParam) {
        case WM_RBUTTONUP:
            SysTray::setState(AppTracker::toggleTracking());
            break;
        case WM_LBUTTONDBLCLK:
            PostQuitMessage(0);
            break;
        default:
           return DefWindowProc(hWnd, msg, wParam, lParam);
        };
        break;
    default:
          return DefWindowProc(hWnd, msg, wParam, lParam);
    };
    return 0;
}

HWND SysTray::hWnd;
//WNDPROC SysTray::glpfnConsoleWindow;
HICON SysTray::iconActive, SysTray::iconInactive;
NOTIFYICONDATAA SysTray::notify;
bool SysTray::active = false;
