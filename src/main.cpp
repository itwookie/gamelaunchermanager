#include "main.h"

namespace pt = boost::property_tree;
using namespace std;

void appGuard();
bool running = true;
thread* appGuardThread;

pt::ptree properties;
#define cfgValueB(P,D) (properties.get<bool>( ( P ), ( D ) ))
#define cfgValueI(P,D) (properties.get<int>( ( P ), ( D ) ))

template <typename T>
T cfgValue (const std::string Path, const T Default) {
//    if (properties.find(Path) == properties.not_found()) {
//        std::cout << "Could not find key " << Path << ", assuming " << Default << std::endl;
//        return Default;
//    } else {
//        return properties.get<T>(Path);
//    }
    auto value = properties.get_optional<T>(Path);
    if (!value) {
        std::cout << "Could not find key " << Path << ", assuming " << Default << std::endl;
        return Default;
    } else {
        std::cout << Path << " = " << value.get() << std::endl;
        return value.get();
    }
}

SysTray tray;

void resetCursor();

int main() {
    appGuardThread = new thread(appGuard);

    pt::read_json("config.json", properties);
    AppTracker::setupTicks((uint16_t)(cfgValue<int>("Config.tickInterval", 2) & 0x0ffff),
                           (uint32_t)(cfgValue<int>("Config.maxIdleTicks", 150) & 0xffffffff));

    AppTracker steam(L"Steam", L"Valve\\Steam", L"InstallPath", L"Steam.exe", {L"steamwebhelper.exe"});
    AppTracker ubisoft(L"Ubisoft Launcher", L"ubisoft\\Launcher", L"InstallDir", L"upc.exe", {L"UplayWebCore.exe"});
    AppTracker origin(L"Origin", L"Origin", L"ClientPath", {L"QtWebEngineProcess.exe"});
    AppTracker bethnet(L"Bethesda.net", L"bethesda softworks\\Bethesda.net", L"installLocation", L"BethesdaNetLauncher.exe", {});
    AppTracker gog(L"GalaxyClient", L"GOG.com\\GalaxyClient\\paths", L"client", L"GalaxyClient.exe", {L"GalaxyClient Helper.exe", L"GOG Galaxy Notifications Renderer.exe"});
    //AppFinder battlenet(L"bethesda softworks\\Bethesda.net", L"installLocation", L"BethesdaNetLauncher.exe", {});
    #ifdef _WIN64
    AppTracker epic(L"Epic Launcher", L"EpicGames\\Unreal Engine", L"INSTALLDIR", L"Launcher\\Portal\\Binaries\\Win64\\EpicGamesLauncher.exe", {L"UnrealCEFSubProcess.exe"});
    #else // _WIN32
    AppTracker epic(L"Epic Launcher", L"EpicGames\\Unreal Engine", L"INSTALLDIR", L"Launcher\\Portal\\Binaries\\Win32\\EpicGamesLauncher.exe", {L"UnrealCEFSubProcess.exe"});
    #endif

    steam.setSticky( cfgValue<bool>("Steam.keepAlive", false) );
    ubisoft.setSticky( cfgValue<bool>("Ubisoft.keepAlive", false) );
    origin.setSticky( cfgValue<bool>("Origin.keepAlive", false) );
    bethnet.setSticky( cfgValue<bool>("Bethesda.keepAlive", false) );
    gog.setSticky( cfgValue<bool>("Galaxy.keepAlive", false) );
    epic.setSticky( cfgValue<bool>("Epic.keepAlive", false) );

    MSG msg;
    while (GetMessage(&msg, nullptr, 0, 0)) {
        TranslateMessage(&msg);
        DispatchMessage(&msg);
    }

    running = false;
    appGuardThread->join();
    delete(appGuardThread);

    return 0;
}

void appGuard() {
    char msg[256] = {0};
    while (running) {
        resetCursor();
        AppTracker::rescan();
        //wcout << steam << ubisoft << origin << bethnet << gog << epic;
        map<wstring, AppTracker*>& tracker = AppTracker::getAllTracker();
        for (auto& x : tracker) {
            if (x.second->tick()) {
                wstring clientName = x.second->getLabel();
                sprintf_s(msg, 255, "%ls was closed", clientName.c_str());
                SysTray::popup(msg);
            }
            wcout << *(x.second);
        }
        wcout << endl;
        wcout << L"  Double-Click the Tray icon to exit" << endl << L"  Right-Click the Tray icon to pause GLM" << endl;
        this_thread::sleep_for(chrono::seconds(::TICK_DELAY));
    }
}

void resetCursor() {
    HANDLE console = GetStdHandle(STD_OUTPUT_HANDLE);//CreateConsoleScreenBuffer( GENERIC_READ | GENERIC_WRITE, 0, NULL, CONSOLE_TEXTMODE_BUFFER, NULL);
    COORD pos = { 0, 0 };
    CONSOLE_SCREEN_BUFFER_INFO screen;
    DWORD written;

    if (console != NULL) {
        GetConsoleScreenBufferInfo(console, &screen);
        FillConsoleOutputCharacterA(
            console, ' ', screen.dwSize.X * screen.dwSize.Y, pos, &written
        );
        SetConsoleActiveScreenBuffer(console);
        SetConsoleCursorPosition(console, pos);
    }
}
